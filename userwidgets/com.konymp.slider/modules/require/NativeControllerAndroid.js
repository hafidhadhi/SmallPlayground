define(['./Inherits','./NativeController','./konyLogger'],function (Inherits,NativeController,konyLoggerModule) {
  var konymp = konymp || {};
  konymp.logger = new konyLoggerModule("NativeControllerAndroid");
  /**
    * @class  NativeControllerAndroid
    * @private
    * @description: Class for the Android implementation of the range slider
    */
   	var NativeControllerAndroid=function(componentInstance){
        konymp.logger.trace("-- Start constructor NativeControllerAndroid --", konymp.logger.FUNCTION_ENTRY);
      	var self=this;
      	self.componentInstance=componentInstance;
      	NativeController.call(this,componentInstance);
      	konymp.logger.trace("-- end  constructor  NativeControllerAndroid --", konymp.logger.FUNCTION_EXIT);
    };
      	
   Inherits(NativeControllerAndroid,NativeController);
  NativeControllerAndroid.prototype.updateDolayout = function(){
      this.componentInstance._doCount=2;
         this.componentInstance.view.flxMain.doLayout = function(){
        this.componentInstance._doCount--;
        if(this.componentInstance._doCount === 0){
          this.componentInstance.view.flxMain.doLayout = function(){};
          this.componentInstance.handler.checkAndCall();
        }
      }.bind(this);
  };
    return NativeControllerAndroid;
});