define(['./konyLogger'],function (KonyLoggerModule){
  var konymp = konymp || {};  
  konymp.slider = konymp.slider || {};
  konymp.logger = (new KonyLoggerModule("Range Slider")) || function(){};
  var NativeController=function(componentInstance,componentName){

    this.componentInstance=componentInstance;
    this._divisions = null;
    this.componentInstance.selectedCursor = "";
    this._errorCodes = {
      errorState:0,
      errorCode:"",
      errorMessage:""
    };
    this._divArray =[];
    this.flag = 0;

    /**
     * @function : getSelectedValues
     * @description : called to get the selected range of slider
     * @return : array of values
     * @remarks : none
     */

    NativeController.prototype.getSelectedValues = function(){
      try{
        return [this.componentInstance.view.lblIndicatorLeft.text,this.componentInstance.view.lblIndicatorRight.text];
      }
      catch(exception){
        konymp.logger.error("#####Exception in getSelectedValues call : " + exception.Msg, konymp.logger.EXCEPTION);
        throw exception;
      }
    };

    /**
     * @function validateRange
     * @description validates the range
     * private
     */
    NativeController.prototype.validateRange = function () {
      konymp.logger.trace("----------Entering validateRange Getter---------", konymp.logger.FUNCTION_ENTRY);
      try {
        this.componentInstance.view.setEnabled(true);
        if(this._errorCodes.errorState===1){
          this.componentInstance.view.setEnabled(false);
          this.componentInstance.view.forceLayout();
          if ((this.componentInstance.onErrorCallback !== undefined  && this.componentInstance.onErrorCallback !== null)){
            this.componentInstance.onErrorCallback({"Error":this._errorCodes.errorMessage});
          }
          return false;
        }
        return true;

      } catch (exception) {
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
        throw(exception);
      }
      konymp.logger.trace("---------------Exiting validateRange function---------------", konymp.logger.FUNCTION_EXIT);
    };
    /**
     * @function checkAndCall
     * @description checks the properties
     * private
     */
    NativeController.prototype.checkAndCall = function(){
      konymp.logger.trace("----------Entering checkAndCall Getter---------", konymp.logger.FUNCTION_ENTRY);
      try{
        this._beginRange = parseInt(this.componentInstance.minValue);
        this._endRange = parseInt(this.componentInstance.maxValue);
        this.componentInstance._minValue = parseInt(this.componentInstance.minValue);
        this.componentInstance._maxValue = parseInt(this.componentInstance.maxValue);
        this.checkProperties();
        this.generateDivs();
      }catch(e){
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
        throw e;
      }
      konymp.logger.trace("----------Exiting checkAndCall Getter---------", konymp.logger.FUNCTION_ENTRY);
    };

    /**
     * @function initGlobals
     * @description sets the initial values of frames
     * private
     */
    NativeController.prototype.initGlobals = function () {

      konymp.logger.trace("----------Entering initGlobals Getter---------", konymp.logger.FUNCTION_ENTRY);
      try {
        this._divArray = [];
        var range = Math.abs(this.componentInstance.minValue - this.componentInstance.maxValue);
        this._divisions = parseInt(range / parseInt(this.componentInstance._minimumIncrementValue));
        this._graduations = this._divisions;
        var widthStr = parseInt(this.componentInstance.view.flxMain.frame.width);
        this._containerWidth = parseInt(widthStr);
        this._sliderOffset = this.xInPercentages(parseInt(this.componentInstance.view.imgBulletLeft.frame.x)+parseInt((this.componentInstance.view.imgBulletLeft.frame.width)/2));
        this._sliderWidthInPercentage = this.xInPercentages(parseInt(this.componentInstance.view.lblFullRange.frame.width));
        this._graduationInterval = (this._endRange - this._beginRange) / this._graduations;
        this._graduationIntervalInPercentage = this._sliderWidthInPercentage / this._graduations;
      } catch (exception) {
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
        throw(exception);
      }
      konymp.logger.trace("----------Exiting initGlobals Getter---------", konymp.logger.FUNCTION_EXIT);
    };

    /**
     * @function generateDivs
     * @description generates divisions as per the inputs
     * @private
     */
    NativeController.prototype.generateDivs = function(){
      konymp.logger.trace("----------Entering generateDivs Getter---------", konymp.logger.FUNCTION_ENTRY);
      try {
        this._beginRange = parseInt(this.componentInstance.minValue);
        this._endRange = parseInt(this.componentInstance.maxValue);
        this.componentInstance._minValue = parseInt(this.componentInstance.minValue);
        this.componentInstance._maxValue = parseInt(this.componentInstance.maxValue);
        this.initGlobals();
        this.setInitialSliderValues();
        if (this.validateRange()) {
          this._interval = (this._endRange - this._beginRange) / this._divisions;
          for (var i = 0; i < (this._divisions + 1); i++) {
            var valInPer = (i / this._divisions) * this._sliderWidthInPercentage;
            this._divArray.push(this._sliderOffset + valInPer);
          }
        } else {
          throw {
            "Message": "Invalid range",
            "Error": "Slider"
          };
        }
      } catch (exception) {
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
      }
      konymp.logger.trace("----------Exiting generateDivs Getter---------", konymp.logger.FUNCTION_ENTRY);
    };
    /**
     * @function setInitialSliderValues
     * @description sets the initial slider values
     * @param val 
     * private
     */
    NativeController.prototype.setInitialSliderValues = function(val) {
      konymp.logger.trace("----------Entering setInitialSliderValues Getter---------", konymp.logger.FUNCTION_ENTRY);
      try {
        if(this.componentInstance.view.lblSelectedLeft.text ==="XXX"){
        this.componentInstance.view.lblSelectedLeft.text = this.componentInstance.minValue;
        this.componentInstance.view.lblSelectedRight.text = this.componentInstance.maxValue;
        }
        this.componentInstance.view.lblIndicatorLeft.text = this.componentInstance.view.lblSelectedLeft.text;
        this.componentInstance.view.lblIndicatorRight.text = this.componentInstance.view.lblSelectedRight.text;
        this.componentInstance.view.imgRightSlider.src=this.componentInstance.selectedRangeEndpointIcon;
        this.componentInstance.view.imgBulletRight.src = this.componentInstance.fullRangeEndpointIcon;
        this.componentInstance.view.lblBegin.skin = this.componentInstance.sknUnselectedIndex;
        this.componentInstance.view.lblEnd.skin = this.componentInstance.sknUnselectedIndex;
        this.componentInstance.view.lblSelectedRange.skin = this.componentInstance.sknSelectedRange;
        this.componentInstance.view.lblSelectedRight.skin = this.componentInstance.sknSelectedIndex;
        this.componentInstance.view.lblIndicatorRight.skin = this.componentInstance.sknSelectedIndex;
        this.componentInstance.view.lblIndicatorLeft.skin = this.componentInstance.sknSelectedIndex;
        this.componentInstance.view.forceLayout();
      } catch (exception) {
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
        throw(exception);
      }
      konymp.logger.trace("----------Exiting setInitialSliderValues Getter---------", konymp.logger.FUNCTION_ENTRY);
    };
    /**
     * @function checkMinAndMaxValues
     * @description checks the properties error handling
     * @param val 
     * @private
     */
    NativeController.prototype.checkMinAndMaxValues = function(val){
      konymp.logger.trace("----------Entering checkMinAndMaxValues Getter---------", konymp.logger.FUNCTION_ENTRY);
      try {
        var err = {};
        if ((val === undefined||val === null)&&this._errorCodes.errorState!= 1 ) {

          err= {
            "code": 2100,
            "message": "Minimum Value or maximum Value Property is null or undefined"
          };
          this._errorCodes.errorState= 1;
          this._errorCodes.errorMessage= err;
        }
        if (isNaN(val)&&this._errorCodes.errorState!= 1) {
          err= {
            "code": 2100,
            "message": "Invalid data type of Minimum Value or Maximum Value property"
          };
          this._errorCodes.errorState= 1;
          this._errorCodes.errorMessage= err;
        }
      } catch (exception) {
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
        throw exception;
      }
      konymp.logger.trace("----------Exiting checkMinAndMaxValues Getter---------", konymp.logger.FUNCTION_EXIT);

    };

    /**
     * @function checkProperties
     * @description checks the properties error handling
     * @private
     */
    NativeController.prototype.checkProperties = function(){
      konymp.logger.trace("----------Entering checkProperties Getter---------", konymp.logger.FUNCTION_ENTRY);
      try {
        var err = {};
        this.checkMinAndMaxValues(this.componentInstance.minValue);
        this.checkMinAndMaxValues(this.componentInstance.maxValue);
        if ((this.componentInstance.minimumIncrementValue === undefined||this.componentInstance.minimumIncrementValue === null || this.componentInstance.minimumIncrementValue <= 0)&&this._errorCodes.errorState!= 1) {
          err= {
            "code": 2100,
            "message": "Invalid value for Minimum Increment Value"
          };
          this._errorCodes.errorState= 1;
          this._errorCodes.errorMessage= err;
        }
        if ((parseInt(this.componentInstance.maxValue)<=parseInt(this.componentInstance.minValue))&&this._errorCodes.errorState!= 1) {
          err = {
            "code": 2100,
            "message": "Invalid Range for the slider. Maximum value should be greater than the minimum value"

          };
          this._errorCodes.errorState= 1;
          this._errorCodes.errorMessage= err;
        }

        if((parseInt(this.componentInstance.maxValue-this.componentInstance.minValue)%(this.componentInstance.minimumIncrementValue)!==0)&&this._errorCodes.errorState!= 1){
          err= {
            "code": 2100,
            "message": "Invalid Minimum Increment Value.The difference between Maximum and Minimum value should be directly divisible by the minimum increment value"
          };
          this._errorCodes.errorState= 1;
          this._errorCodes.errorMessage= err;
        }
      } catch (exception) {
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
        throw exception;
      }
      konymp.logger.trace("----------Exiting checkProperties Getter---------", konymp.logger.FUNCTION_EXIT);
    };

    /**
     * @function xInPercentages
     * @description converts dp to percentage
     * @param x 
     * @private
     */
    NativeController.prototype.xInPercentages = function(x) {
      konymp.logger.trace("----------Entering xInPercentages Getter---------", konymp.logger.FUNCTION_ENTRY);
      try {
        return (x / this._containerWidth * 100);
      } catch (exception) {
        konymp.logger.error("#####Exception in xInPercentages setter : " + exception.Msg, konymp.logger.EXCEPTION);
      }
      konymp.logger.trace("----------Exiting xInPercentages Getter---------", konymp.logger.FUNCTION_EXIT);
    };

    /**
     * @function onTouchValidate
     * @description Check if the position is within valid range
     * @private
     */
    NativeController.prototype.onTouchValidate = function(x) {
      konymp.logger.trace("----------Entering onTouchValidate function---------", konymp.logger.FUNCTION_ENTRY);
      try {
        if (this.componentInstance.selectedCursor !== "") {
          if (this.xInPercentages(x) >= this.xInPercentages((parseInt(this.componentInstance.view.lblBegin.frame.x)+parseInt(this.componentInstance.view.lblBegin.frame.width)/2))&& this.xInPercentages(x) <= this.xInPercentages((parseInt(this.componentInstance.view.lblEnd.frame.x)+parseInt(this.componentInstance.view.lblEnd.frame.width)/2))) {
            if (Math.round(Math.abs(parseFloat(parseFloat(this.componentInstance.view.flxRight.frame.x)+(parseFloat(this.componentInstance.view.flxRight.frame.width)/2)) - parseFloat(parseFloat(this.componentInstance.view.flxLeft.frame.x)+(parseFloat(this.componentInstance.view.flxLeft.frame.width)/2)))) >= (this._graduationIntervalInPercentage)) {
              this.onTouchSlide(x);
            } else {
              var xLeft = parseInt((this.xInPercentages(x) - this._sliderOffset) / this._graduationIntervalInPercentage);
              if (xLeft == this._divisions) {
                xLeft = xLeft - 1;
              }
              if (xLeft < 0) {
                xLeft = 0;
              }
              var indicatorTextLeft = "" + parseInt(parseInt(this.componentInstance.minValue) + parseInt(xLeft) * (this._graduationInterval));
              var xRight = xLeft + 1;
              var indicatorTextRight = "" + parseInt(parseInt(this.componentInstance.minValue) + parseInt(xRight) * (this._graduationInterval));
              this.componentInstance.view.flxLeft.centerX = parseFloat(this._sliderOffset + xLeft * this._graduationIntervalInPercentage) + "%";
              this.componentInstance.view.lblIndicatorLeft.text = indicatorTextLeft.toString();
              this.componentInstance.view.flxRight.centerX = parseFloat(this._sliderOffset + xRight * this._graduationIntervalInPercentage) + "%";
              this.componentInstance.view.lblIndicatorRight.text = indicatorTextRight.toString();
              this.componentInstance.view.forceLayout();
              this.setLblRange();
              this.componentInstance.selectedCursor = "";
            }

          } else if (this.xInPercentages(x) < this._divArray[0]) {
            this.changeSknNumbers();
            this.componentInstance.view.flxLeft.centerX = this._sliderOffset + "%";
          }
        }
      } catch (exception) {
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
        throw(exception);
      }
      konymp.logger.trace("---------------Exiting createLabels function---------------", konymp.logger.FUNCTION_EXIT);
    };
    /**
     * @function onTouchSlide
     * @description Slides to the required position if the position is in valid range
     * @private
     * @param
     */
    NativeController.prototype.onTouchSlide = function(x) {
      konymp.logger.trace("----------Entering onTouchSlide function---------", konymp.logger.FUNCTION_ENTRY);
      try {
        var selectedFlx;
        var centerXPer = this.xInPercentages(x);
        var indicatorLabel;
        if (this.componentInstance.selectedCursor === "left") {
          selectedFlx = this.componentInstance.view.flxLeft;
          indicatorLabel = this.componentInstance.view.lblIndicatorLeft;
          this.onTouchSlideCulminate(x, centerXPer, selectedFlx, indicatorLabel);
        } else if (this.componentInstance.selectedCursor === "right") {
          selectedFlx = this.componentInstance.view.flxRight;
          indicatorLabel = this.componentInstance.view.lblIndicatorRight;
          this.onTouchSlideCulminate(x, centerXPer, selectedFlx, indicatorLabel);
        }
      } catch (exception) {
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
        throw(exception);
      }
      konymp.logger.trace("---------------Exiting onTouchSlide function---------------", konymp.logger.FUNCTION_EXIT);
    };
    /**
     * @function setLblRange
     * @description sets the lblrange of the slider
     * @private
     */
    NativeController.prototype.setLblRange = function () {
      try {
        konymp.logger.trace("----------Entering setLblRange function---------", konymp.logger.FUNCTION_ENTRY);
        this.componentInstance.view.lblSelectedRange.left = (parseFloat(this.componentInstance.view.flxLeft.centerX)) + "%";
        this.componentInstance.view.lblSelectedRange.width = ((parseFloat(this.componentInstance.view.flxRight.centerX)) - parseFloat(this.componentInstance.view.lblSelectedRange.left)).toString() + "%";
      } catch (exception) {
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
        throw(exception);
      }
      konymp.logger.trace("---------------Exiting setLblRange function---------------", konymp.logger.FUNCTION_EXIT);
    };
    /**
     * @function onTouchSlideCulminate
     * @description sets the centerX of the left or right cursor(selectedFlx)
     * @private
     * @param
     */
    NativeController.prototype.onTouchSlideCulminate = function( x, centerXPer, selectedFlx, indicatorLabel) {
      konymp.logger.trace("----------Entering onTouchSlideCulminate function---------", konymp.logger.FUNCTION_ENTRY);
      try {
        if (!(centerXPer < this._divArray[0] || centerXPer > this._divArray[this._divisions])) {
          var xClose = Math.round((centerXPer - this._sliderOffset) / this._graduationIntervalInPercentage);
          var indicatorText = "" + parseInt(parseInt(this.componentInstance.minValue) + parseInt(xClose) * (this._graduationInterval));
          selectedFlx.centerX = centerXPer + "%";
          this.onTouchSlideEventAndLabelHandler( x, indicatorLabel, indicatorText);
          selectedFlx.forceLayout();
        }
        this.setLblRange();
        this.componentInstance.view.forceLayout();
      } catch (exception) {
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
        throw(exception);
      }
      konymp.logger.trace("---------------Exiting onTouchSlideCulminate function---------------", konymp.logger.FUNCTION_EXIT);
    };

    /**
     * @function onTouchSlideEventAndLabelHandler
     * @description handles the events and label skin changes
     * @private
     * @param x
     * @param indicatorLabel
     * @param indicatorText
     */
    NativeController.prototype.onTouchSlideEventAndLabelHandler = function(x, indicatorLabel, indicatorText) {
      konymp.logger.trace("----------Entering onTouchSlideEventAndLabelHandler function---------", konymp.logger.FUNCTION_ENTRY);
      try {
        if (this._bufferSelectedLeft !== null && this._bufferSelectedLeft !== indicatorText && this.componentInstance.selectedCursor === "left") {
          indicatorLabel.text = indicatorText;
          this._bufferSelectedLeft = indicatorText;
          if (this.componentInstance.onRangeChange !== undefined  && this.componentInstance.onRangeChange !== null) {
            this.componentInstance.onRangeChange([this.componentInstance.view.lblIndicatorLeft.text, this.componentInstance.view.lblIndicatorRight.text]);
          }
          this.componentInstance.view.forceLayout();
        } 
        else if (this._bufferSelectedRight !== null && this._bufferSelectedRight !== indicatorText && this.componentInstance.selectedCursor === "right") {
          indicatorLabel.text = indicatorText;
          this._bufferSelectedRight = indicatorText;
          if (this.componentInstance.onRangeChange !== undefined  && this.componentInstance.onRangeChange !== null) {
            this.componentInstance.onRangeChange([this.componentInstance.view.lblIndicatorLeft.text, this.componentInstance.view.lblIndicatorRight.text]);
          }
        }
        this.changeSknNumbers();
        this.componentInstance.view.forceLayout();
      } catch (exception) {
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
        throw(exception);
      }
      konymp.logger.trace("---------------Exiting onTouchSlideEventAndLabelHandler function---------------", konymp.logger.FUNCTION_EXIT);
    };

    /**
     * @function onSnapValidate
     * @description checks if the snap to position is valid or not
     * @private
     */
    NativeController.prototype.onSnapValidate = function(x) {
      konymp.logger.trace("----------Entering onSnapValidate function---------", konymp.logger.FUNCTION_ENTRY);
      try {
        this.flag = true;
        var xLeft = parseInt((this.xInPercentages(x) - this._sliderOffset) / this._graduationIntervalInPercentage);
        var diff = (Math.round(parseFloat(this.componentInstance.view.flxRight.centerX) - parseFloat(this.componentInstance.view.flxLeft.centerX)));
        if (diff >= (this._graduationIntervalInPercentage)) {
          this.snapToPosition(x);
        } else {
          if (xLeft >= this._graduations) {
            xLeft = this._graduations - 1;
          }
          if (xLeft < 0) {
            xLeft = 0;
          }
          var tempxL = parseInt(xLeft) * parseInt(this._graduationInterval);
          var xRight = parseInt(xLeft) + 1;
          var tempxR = parseInt(xRight) * parseInt(this._graduationInterval);
          var indicatorTextLeft = (parseInt(this.componentInstance.minValue) + tempxL)+"";
          var indicatorTextRight =  (parseInt(this.componentInstance.minValue) + tempxR)+"";
          this.componentInstance.view.flxLeft.centerX = (this._sliderOffset + xLeft * this._graduationIntervalInPercentage) + "%";
          this.componentInstance.view.lblIndicatorLeft.text = indicatorTextLeft;
          this.componentInstance.view.flxRight.centerX = (this._sliderOffset + xRight * this._graduationIntervalInPercentage) + "%";
          this.componentInstance.view.lblIndicatorRight.text = indicatorTextRight;
          this.setLblRange();
          this.componentInstance.selectedCursor = "";
          this.snapToPosition(x);
        }
      } catch (exception) {
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
        throw(exception);
      }
      konymp.logger.trace("---------------Exiting onSnapValidate function---------------", konymp.logger.FUNCTION_EXIT);
    };

    /**
     * @function snapToPosition
     * @description Calculates which step is the closest
     * @private
     */
    NativeController.prototype.snapToPosition = function(x) {
      konymp.logger.trace("----------Entering snapToPosition function---------", konymp.logger.FUNCTION_ENTRY);
      try {
        var rem = (this.xInPercentages(x) - this._sliderOffset) % this._graduationIntervalInPercentage;
        var mid = this._graduationIntervalInPercentage / 2;
        var tempX;
        if (rem < mid) {
          tempX = this.xInPercentages(x) - rem;
        } else {
          tempX = this.xInPercentages(x) + (this._graduationIntervalInPercentage - rem);
        }
        if (tempX > this._divArray[this._divisions]) {
          tempX = this._divArray[this._divisions];
        }
        var xClose = parseInt(Math.round((tempX - this._sliderOffset) / this._graduationIntervalInPercentage));
        var indicatorText = "" + parseInt(parseInt(this.componentInstance.minValue) + parseInt(xClose) * (this._graduationInterval));
        this.snapLeftOrRight(x, tempX, indicatorText);
      } catch (exception) {
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
        throw(exception);
      }
      konymp.logger.trace("---------------Exiting snapToPosition function---------------", konymp.logger.FUNCTION_EXIT);
    };
    /**
     * @function snapLeftOrRight
     * @description snaps left or right depending on which is closest
     * @private
     * @param x -in dp,tempX s in percentages,indicatorText -value displayed by indicator
     */
    NativeController.prototype.snapLeftOrRight = function(x, tempX, indicatorText) {
      konymp.logger.trace("----------Entering snapLeftOrRight function---------", konymp.logger.FUNCTION_ENTRY);
      try {
        if (!(tempX < this._divArray[0] || tempX > this._divArray[this._divisions])) {
          var positionCen = tempX + "%";
          var leftIndicator = parseFloat(this.componentInstance.view.flxLeft.centerX);
          var rightIndicator = parseFloat(this.componentInstance.view.flxRight.centerX);
          if (Math.abs((tempX) - (leftIndicator)) <= Math.abs((rightIndicator) - tempX)) {
            if (Math.round(rightIndicator - leftIndicator) >= this._graduationIntervalInPercentage) {
              this.componentInstance.view.flxLeft.centerX = positionCen;
              this.componentInstance.view.flxLeft.lblIndicatorLeft.text = indicatorText;
              if ((this.componentInstance.onRangeChangeEnd !== undefined && this.componentInstance.onRangeChangeEnd !==null )){ 
                this.componentInstance.onRangeChangeEnd([this.componentInstance.view.lblIndicatorLeft.text, this.componentInstance.view.lblIndicatorRight.text]);
              }
            }
          } 
          else {
            if (Math.round(rightIndicator - leftIndicator) >= this._graduationIntervalInPercentage) {
              this.componentInstance.view.flxRight.centerX = positionCen;
              this.componentInstance.view.flxRight.lblIndicatorRight.text = indicatorText;
              if ((this.componentInstance.onRangeChangeEnd !== undefined  && this.componentInstance.onRangeChangeEnd !== null)){
                this.componentInstance.onRangeChangeEnd([this.componentInstance.view.lblIndicatorLeft.text, this.componentInstance.view.lblIndicatorRight.text]);
              }
            }
          }
          this.componentInstance.view.forceLayout();
          this.changeSknNumbers();
          this.setLblRange();
          this.componentInstance.view.forceLayout();
        }
      } catch (exception) {
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
        throw(exception);
      }
      konymp.logger.trace("---------------Exiting snapLeftOrRight function---------------", konymp.logger.FUNCTION_EXIT);
    };
    /**
     * @function PinIndicatorIsEnabled
     * @description animates the indicator of slider
     * @param val 
     * @param obj1 
     * @param obj 
     */
    NativeController.prototype.PinIndicatorIsEnabled = function (val,obj1, obj) {
      konymp.logger.trace("----------Entering startIndexValidateAndSet function---------", konymp.logger.FUNCTION_ENTRY);
      try {
        obj.isVisible = true;
        var transformObject1 = kony.ui.makeAffineTransform();
        transformObject1.scale(0, 0);
        var transformObject2 = kony.ui.makeAffineTransform();
        transformObject2.scale(1.2, 1.2);
        var transformObject3 = kony.ui.makeAffineTransform();
        transformObject3.scale(1, 1);
        if ( val === undefined || val === null) {
          throw {
            "Error": "Slider",
            "message": "Invalid data type argument for PinIndicatorIsEnabled function"
          };
        }
        if (!val) {
          var temp = transformObject1;
          transformObject1 = transformObject3;
          transformObject3 = temp;
        }
        var imgAnim = kony.ui.createAnimation({
          "0": {
            "stepConfig": {
              "timingFunction": kony.anim.EASE
            },
            "transform": transformObject1
          },
          "80": {
            "stepConfig": {
              "timingFunction": kony.anim.EASE
            },
            "transform": transformObject2
          },
          "100": {
            "stepConfig": {
              "timingFunction": kony.anim.EASE
            },
            "transform": transformObject3
          }
        });
        var animConfig1 = {
          "delay": 0.1,
          "iterationCount": 1,
          "fillMode": kony.anim.FILL_MODE_FORWARDS,
          "duration": 0.25
        };
        obj.animate(imgAnim, animConfig1);
      } catch (exception) {
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
        throw(exception);
      }
      konymp.logger.trace("---------------Exiting PinIndicatorIsEnabled function---------------", konymp.logger.FUNCTION_EXIT);
    };
    /**
     * @function changeSknNumbers
     * @description handles the skin chnages of the labels
     */
    NativeController.prototype.changeSknNumbers = function () {
      try {
        konymp.logger.trace("----------Entering changeSknNumbers function---------", konymp.logger.FUNCTION_ENTRY);
        var objL = this.componentInstance.view.lblSelectedLeft;
        var objR = this.componentInstance.view.lblSelectedRight;
        if(this.componentInstance.view.lblIndicatorLeft.text == this.componentInstance.view.lblBegin.text){
          this.componentInstance.view.lblBegin.opacity=0;
          objL.text = this.componentInstance.view.lblIndicatorLeft.text;
          objL.isVisible=true;
        }
        else{
          objL.text = this.componentInstance.view.lblIndicatorLeft.text;
          objL.isVisible=true;
          this.componentInstance.view.lblBegin.opacity=1;

        }
        if(this.componentInstance.view.lblIndicatorRight.text == this.componentInstance.view.lblEnd.text){
          this.componentInstance.view.lblEnd.opacity=0;
          objR.text = this.componentInstance.view.lblIndicatorRight.text;
          objR.isVisible=true;
        }
        else{
          objR.text = this.componentInstance.view.lblIndicatorRight.text;
          objR.isVisible=true;
          this.componentInstance.view.lblEnd.opacity=1;
        }
      } catch (exception) {
        konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
        throw(exception);
      }
      konymp.logger.trace("---------------Exiting changeSknNumbers function---------------", konymp.logger.FUNCTION_EXIT);
    };

  };

  return NativeController;
});