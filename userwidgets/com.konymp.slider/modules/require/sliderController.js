define(['./ControllerImplementation','./konyLogger'],function(ControllerImplementation,konyLogger) {

  var konymp = konymp || {};
  konymp.logger = (new konyLogger("RangeSlider")) || function() {};
  konymp.logger.setLogLevel("DEBUG");
  konymp.logger.enableServerLogging = true;
  return {
    /**
     * @function
     *
     * @param baseConfig 
     * @param layoutConfig 
     * @param pspConfig 
     */
    constructor: function(baseConfig, layoutConfig, pspConfig) {
			var analytics=require("com/konymp/"+"slider"+"/analytics");
            analytics.notifyAnalytics();
      this._minValue = null;
      this._maxValue = null;
      this._minimumIncrementValue = null;
      this.handler=new ControllerImplementation(this,baseConfig.id);
      this.view.lblSelectedRange.width = this.view.lblFullRange.width;
      this.view.lblSelectedLeft.text = "XXX";
      this.view.lblSelectedRight.text = "XXX";
      this.view.lblBegin.opacity = 0;
      this.view.lblEnd.opacity = 0;
      this.view.lblSelectedLeft.isVisible = true;
      this.view.lblSelectedRight.isVisible = true;
    },

    /**
     * @function
     *
     */
    initGettersSetters: function() {
      konymp.logger.trace("----------Entering initGettersSetters function---------", konymp.logger.FUNCTION_ENTRY);
      /**
       * @property : minimumIncrementValue
       * @description : this property set the minimum increment value of slider
       * @return : none
       * @remarks : none
       */
      defineSetter(this, "minimumIncrementValue", function(divisions) {
        konymp.logger.trace("----------Entering minimumIncrementValue Setter function---------", konymp.logger.FUNCTION_ENTRY);
        try {
          this.view.lblSelectedLeft.text = "XXX";
          this.view.lblSelectedRight.text = "XXX";
          if(this._minimumIncrementValue === null){
            this._minimumIncrementValue = divisions;
          }
          else{
            this._minimumIncrementValue = divisions;
            this.handler.checkAndCall();
          }
          this.view.forceLayout();
        } catch (exception) {
          konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
          if (exception.Error === "SliderComponent") {
            throw exception;
          }
        }

        konymp.logger.trace("---------------Exiting minimumIncrementValue Setter function---------------", konymp.logger.FUNCTION_EXIT);
      });
      defineGetter(this, "minimumIncrementValue", function() {
        konymp.logger.trace("----------Entering minimumIncrementValue getter function---------", konymp.logger.FUNCTION_ENTRY);
        return this._minimumIncrementValue;
      });
      /**
       * @property : minValue
       * @description : this property set the minimum value of slider
       * @return : none
       * @remarks : none
       */
      defineSetter(this, "minValue", function(val) {
        konymp.logger.trace("----------Entering minValue Setter function---------", konymp.logger.FUNCTION_ENTRY);
        try {
          this.view.lblSelectedLeft.text = "XXX";
          this.view.lblSelectedRight.text = "XXX";
          if(this._minValue === null){
            this._minValue = val;
            this.view.lblBegin.text=val+"";
          }
          else{
            this._minValue = val;
            this.view.lblBegin.text=val+"";
            this.handler.checkAndCall();
          }
          this.view.forceLayout();
        }
        catch (exception) {
          if (exception.Error === "SliderComponent") {
            konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
            throw exception;
          }
        }
        konymp.logger.trace("----------Exiting minValue Setter function---------", konymp.logger.FUNCTION_ENTRY);
      });
      /**
       * @property : maxValue
       * @description : this property set the maximum value of slider
       * @return : none
       * @remarks : none
       */
      defineSetter(this, "maxValue", function(val) {
        konymp.logger.trace("----------Entering minValue Setter function---------", konymp.logger.FUNCTION_ENTRY);
        try {
          this.view.lblSelectedLeft.text = "XXX";
          this.view.lblSelectedRight.text = "XXX";
          if(this._maxValue === null){
            this._maxValue = val;
            this.view.lblEnd.text=val+"";
          }
          else{
            this._maxValue = val;
            this.view.lblEnd.text=val+"";
            this.handler.checkAndCall();
          }
          this.view.forceLayout();
        } catch (exception) {
          if (exception.Error === "SliderComponent") {
            konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
            throw exception;
          }
        }
        konymp.logger.trace("----------Exiting minValue Setter function---------", konymp.logger.FUNCTION_ENTRY);
      });
    },

    /**
     * @api : getSelectedValues
     * @description : called to get the selected range of slider
     * @return : array of values
     * @remarks : none
     */

    getSelectedValues : function(){
      return this.handler.getSelectedValues();
    },
    /**
     * @api : resetRangeSlider
     * @description : called to reset slider dynamically
     * @return : none
     * @remarks : none
     */

    resetRangeSlider : function(){
      this.handler.updateDolayout();
    },
    
     /**
     * @api : initialize
     * @description : This is used to initialize the properties of the Component When created dynamically.
     * @return : none
     * @remarks : none
     */
    initialize : function(){
       kony.timer.schedule("rangeSlider_preShow", function(){
        this.handler.getNativeController().checkAndCall();
        this.view.forceLayout();
        kony.timer.cancel("rangeSlider_preShow");
      }.bind(this) , 0.1, false);
    },

    /**
     * @event : onErrorCallback
     * @description : Called if any error occurs in the component
     * @param : error {object} 
     */

    onErrorCallback : function(error){
      //defining
      alert(JSON.stringify(error));
    },

    /**
     * @event : onRangeChangeEnd
     * @description : Called at the end of range change
     * @param : values array
     */
    onRangeChangeEnd : function(){
      //defining
    },
    /**
     * @event : onRangeChange
     * @description : Called when the range gets changed
     * @param : values array
     */
    onRangeChange:function(){
      //defining
    }

  };
});