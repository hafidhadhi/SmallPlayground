define(['./Inherits','./NativeController','./konyLogger'],function (Inherits,NativeController,konyLoggerModule) {
  //logger for the range slider ios controller
  var konymp = konymp || {};
  konymp.logger = new konyLoggerModule("NativeControllerIOS");

  /**
    * @class  NativeControllerIOS
    * @private
    * @description: Class for the IOS implementation of the DAON
    */
  var NativeControllerIOS=function(componentInstance){
    konymp.logger.trace("-- Start constructor NativeControllerIOS --", konymp.logger.FUNCTION_ENTRY);
    var self = this;
    self.componentInstance=componentInstance;
    NativeController.call(this,componentInstance);
	konymp.logger.trace("-- end  constructor  NativeControllerIOS --", konymp.logger.FUNCTION_EXIT);

  };
  Inherits(NativeControllerIOS,NativeController);
  NativeControllerIOS.prototype.updateDolayout = function(){
      this.componentInstance._doCount=2;
         this.componentInstance.view.imgBulletRight.doLayout = function(){
        this.componentInstance._doCount--;
        if(this.componentInstance._doCount === 0){
          this.componentInstance.view.imgBulletRight.doLayout = function(){};
          this.componentInstance.handler.checkAndCall();
        }
      }.bind(this);
  };
  return NativeControllerIOS;
});
