define([],function (){
  var ControllerImplementation=function(componentInstance,componentName){
    this.componentInstance=componentInstance;
    this.getNativeController=function(){
      if(this.nativeControllerInstance===undefined){
        var deviceInfo=kony.os.deviceInfo();
        var platformName=null;
        if(deviceInfo.name.toLowerCase()==='iphone' || deviceInfo.name.toLowerCase()==='ipad')
        {
          platformName='IOS.js';
        }else if(deviceInfo.name.toLowerCase()==='android'){
          platformName='Android.js';
        }else{
          platformName=deviceInfo.name.charAt(0).toUpperCase()+deviceInfo.name.slice(1);
        }
        var nativeControllerPath='com/konymp/slider'+'/NativeController'+platformName;
        var nativeController=require(nativeControllerPath);
        this.nativeControllerInstance=new nativeController(componentInstance);
      }
      return this.nativeControllerInstance;
    };
    /**
     * @api : getSelectedValues
     * @description : called to get the selected range of slider
     * @return : array of values
     * @remarks : none
     */

    this.getSelectedValues = function(){
      return this.getNativeController().getSelectedValues();
    };

   
    /**
     * @function checkAndCall
     * @description checks the properties
     * private
     */
    this.checkAndCall = function(){
      this.getNativeController().checkAndCall();
     };


   /**
		 * @function onTouchValidate
		 * @description Check if the position is within valid range
		 * @private
		 */
    this.onTouchValidate = function(x) {
      this.getNativeController().onTouchValidate(x);
     };
    /**
		 * @function onTouchSlide
		 * @description Slides to the required position if the position is in valid range
		 * @private
		 * @param
		 */
    this.onTouchSlide = function(x) {
      this.getNativeController().onTouchSlide(x);
     };

    /**
		 * @function onSnapValidate
		 * @description checks if the snap to position is valid or not
		 * @private
		 */
    this.onSnapValidate = function(x) {
      this.getNativeController().onSnapValidate(x);
      };

    /**
     * @function PinIndicatorIsEnabled
     * @description animates the indicator of slider
     * @param val 
     * @param obj1 
     * @param obj 
     */
    this.PinIndicatorIsEnabled = function (val,obj1, obj) {
      this.getNativeController().PinIndicatorIsEnabled(val,obj1, obj);
    };
    
    this.updateDolayout = function(){
      this.getNativeController().updateDolayout();
  };
  };

  return ControllerImplementation;
});