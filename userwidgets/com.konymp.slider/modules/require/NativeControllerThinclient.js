define(['./Inherits','./NativeController','./konyLogger'],function (Inherits,NativeController,konyLoggerModule) {
  //logger for the range slider ios controller
  var konymp = konymp || {};
  konymp.logger = new konyLoggerModule("NativeControllerThinclient");

  /**
    * @class  NativeControllerThinclient
    * @private
    * @description: Class for the IOS implementation of the range slider
    */
  var NativeControllerThinclient=function(componentInstance){
    konymp.logger.trace("-- Start constructor NativeControllerThinclient --", konymp.logger.FUNCTION_ENTRY);
    var self = this;
    self.componentInstance=componentInstance;
    NativeController.call(this,componentInstance);
	konymp.logger.trace("-- end  constructor  NativeControllerThinclient --", konymp.logger.FUNCTION_EXIT);

  };
  Inherits(NativeControllerThinclient,NativeController);
  NativeControllerThinclient.prototype.updateDolayout = function(){
      this.componentInstance._doCount=1;
      this.componentInstance.view.imgBulletRight.doLayout = function(){
        this.componentInstance._doCount--;
        if(this.componentInstance._doCount === 0){
          this.componentInstance.view.imgBulletRight.doLayout = function(){};
          this.componentInstance.handler.checkAndCall();
        }
      }.bind(this);
  };
  return NativeControllerThinclient;
});
 